---
title: "Less Is More"
date: 2021-04-07T10:17:11-04:00
Description: ""
Tags: ["Programming", "WordPress"]
draft: false
---

So often when you hire a new developer, they get very excited about telling you how many hundreds or thousands of lines of code they've written to your end goal.

## What your goal as a programmer should be:
Sometimes, writing many lines of code is unavoidable and the effort can be applauded. However, in the great majority of cases, *especially* when using a heavy, bloated piece of software like the WordPress CMS, less is more. A programmer should try to be as brief as possible **without** sacrificing readability of the code. Being able to say at the end of the day, "I solved the problem with 10 lines of code, and every single line was understandable, logical and easily maintainable" is much better than being able to say, "I solved the problem with 100 lines of code with extensive comments and unnecessary additional files." The next developer who comes along will appreciate the brevity of the first developer, whose work is invisible and indistinguishable from the rest of the project, rather than have to trudge through the excessive code of the second.

## The greatest thing a programmer can do:
If writing less lines of code is admirable, the epitomy of a good programmer is one who reduces the lines of excess code. At the end of the day, if you can say, "I removed X lines of code from the project, and maintained the functionality, while improving readability," then good job and thank you! Now when your successor comes along, he will have X less lines of code to read, and will be able to solve the problems he has that much quicker.
