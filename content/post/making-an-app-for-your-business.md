---
title: "Making an App for Your Business"
date: 2021-02-23
tags: ["DIY", "App development", "Cordova", "Course", "app", "business"]
draft: false
---

**Making an app for your business does not need to be a costly affair.** In fact, I can show you how to make a simple web app, indistinguishable from the expensive mobile apps in a matter of hours, and for less than $200.

Watch the video below to get started.

{{<odysee "making-an-app-part1/2f9ca9e8aa21493ba5e8f43f47a58de643ab2733?r=4azqJWW87nRNznr1ytwYBuTSv9kaKawn">}}

{{<empty_space>}}
### Enjoying the course?
If you're enjoying the course, the full course is available on hotmart. [**Click here to see the course.**]({{< param "courseUrl" >}})

