---
title: "How to Use Gmail With Django"
date: 2021-03-10
tags: ["DIY", "Python", "Django", "Programming"]
draft: false
---
If you've ever set up an email account for use on a mail form (or other general mail sending usage), it is typically very straightforward. You take your login details and the SMTP details for your email provider and plug them in.

## How you are used to it working

In Django, you go into your settings.py, and you set up some variables:
{{< highlight python >}}
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.zoho.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'my-email-address'
EMAIL_HOST_PASSWORD = 'my-email-password'
DEFAULT_FROM_EMAIL = 'the-email-address-I-want-to-appear-on-the-email'
{{< /highlight >}}

## How Google does it

With Google, however, because of account security and two-step verification, it does not work quite so simply. The first step you're going to need to do is setup an [app password](https://support.google.com/accounts/answer/185833?hl=en).

After generating your new app password, setting up gmail becomes trivial. Enter your settings.py, just as you would with any other email provider. Your settings will now look something like this:
{{< highlight python >}}
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'my-email-address'
EMAIL_HOST_PASSWORD = 'my-newly-generated-app-password'
DEFAULT_FROM_EMAIL = 'the-email-address-I-want-to-appear-on-the-email'
{{< /highlight >}}

This tip works for any application or programming language. For example, if you are a fan of "insecure" (Google's way of saying "unapproved") mail software, such as mutt, you'll need to generate an app password. I hope that saves you some time and headache!

