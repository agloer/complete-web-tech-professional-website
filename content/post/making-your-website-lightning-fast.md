---
title: "Making Your Website Lightning Fast"
date: 2021-03-03
tags: ["website", "speed", "WordPress", "SEO"]
draft: false
---
Is your website fast? You may think it is, but according to a [Google study][1], 53% of mobile site visitors will bounce if your page takes longer than 3 seconds to load. Not only have studies shown a decrease in sales, but it can affect your search engine placement!

### How do I determine if I have a problem?
Go to Google's [PageSpeed Insights][2] and test your website. Test your front page and your landing page (the page which you get most of your search engine traffic). If you're in the green, relax. If you're not, read on!

### Ok, so I have a problem, now what?
Scroll down on the [PageSpeed Insights][2] and look at the opportunities there are for your website. A very common problem, especially with websites made by popular CMS's, like WordPress are an excess of CSS files or JavaScript files.

Here are the steps you should follow to reduce the page load times:
1. Remove unnecessary JavaScript code or CSS. If it's not needed for the page, it shouldn't be there!
2. CSS and JavaScript which are 100% necessary for the loading of the page can be placed inside the document.
	* Extract CSS class definitions which are important and place them in a style block in the head of the document. Click [here][4] for an excellent guide on the subject.
	* Critical JavaScript necessary for loading the page can be placed at the bottom of the HTML document.
2. By default all JavaScript is parser blocking. If you have non-critical JavaScript, don't let it slow down the loading of your website. Mark the javascript with an [async attribute][3].
3. Finally, load all less important CSS [asynchronously][4].

Some CMS's do have plugins to help you accomplish this (WP Rocket advertises itself as a solution to speed problems). However, the best thing to do is to manually handle the problem. If you are incapable of handling the problem yourself, consider hiring a professional!

[1]: <https://www.blog.google/products/admanager/increase-speed-of-your-mobile-site-wi/>
[2]: <https://developers.google.com/speed/pagespeed/insights/>
[3]: <https://developers.google.com/web/fundamentals/performance/critical-rendering-path/adding-interactivity-with-javascript>
[4]: <https://web.dev/defer-non-critical-css/>
