---
title: "When to Use jQuery"
date: 2021-03-16
tags: ["website", "speed", "WordPress", "SEO", "jQuery", "bootstrap", "Javascript", "Programming"]
draft: false
---

Oftentimes within the professional programming community, the consensus among Javascript developers is that there is **no reason to use jQuery** in 2021. It's a good guideline to follow, but it is incorrect, and here's why.

### Why is jQuery so bad?
The main problem is bloat. jQuery adds sometimes 30-80kB of data to a page. That doesn't sound like a lot, but as I mentioned in an earlier post, [every second counts]({{< ref "/post/making-your-website-lightning-fast.md" >}})! If you can do the code in pure javascript, it's going to be faster and lighter for the end user!

### So when should I use jQuery?
There are certain situations where you can feel free to use jQuery.
* You are using Bootstrap.
* You are using WordPress.
* You are already using jQuery for another critical element in the page, and it will not add to overhead.

Both WordPress and Bootstrap ship with jQuery already being loaded. You can't remove them from the page, so might as well take advantage of the convenience of the jQuery library. Sometimes instead of running a forEach on a *document.querySelectorAll('')*, if jQuery is already on the page, it makes sense to just use *$("#element").on('click', someFunction)*.

Another use case is when there is an importance of having your code usable in old browsers, such as ancient versions of Internet Explorer. You need some functionality which does not readily exist in older versions of Javascript (or you don't have the time to work it out for all versions of ancient browsers).

Sometimes, in the real world, we don't have the time to do everything in a textbook way. Whenever I have the time, I always like to make my Javascript clean and pure. However, many times, especially with smaller companies or freelancing, when every second counts, your best option is to use jQuery. It's not only going to get the job done, it'll help you to get the job done correctly and focused on reaching your deadlines.

