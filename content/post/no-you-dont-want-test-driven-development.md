---
title: "No, You Dont Want Test Driven Development!"
date: 2021-05-22T08:41:03-04:00
Tags: ["Programming", "Test Driven Development"]
draft: false
---

In today's fast paced world of tech jargon, I see lots of recruitment emphasis placed on "test driven development." It's very hot right now, but it's stupid, and you don't want it!

### Is there ever a place for Test Driven Development? 
In academia, TDD (test driven development) has a real and legitimate purpose. The teacher can provide the tests which save him a lot of time grading assignments. It also helps the student to make sure their code meets the goals of the given assignment. The real world, however, is not academia. The problem with TDD in the real world is time and money. No one ever has enough time or money to do TDD correctly, and if you're not doing it correctly, you're better off not doing it at all.

I've seen your code bases: millions of lines of code in a monolithic app with pages long SQL queries... TDD helps the developer make sure that their code doesn't break the parts of the project that are covered by the tests; it does not ensure that the code added to the project is clean and well-written code. Time spent writing tests is time that is not spent cleaning up your spaghetti and legacy code, and reworking databases so that your queries don't cost you several cents per SQL call.

### What about Agile software development?
This leads me to the next hot tech word du jour, "Agile software development." You put TDD together with Agile, and recruiters have an orgasm, but it's garbage and it shows a lack of real world knowledge. TDD takes time. Since in the real world we have deadlines and time is money, TDD is often taking away time from new code development and legacy code cleanup. So to make sure that developers are hitting deadlines, managers come up with new time management strategies. They introduce concepts like "sprints", which basically means, "get the project done and out the door!" Obviously that means that time is now going to be focused on what it should have been focused on all along, the real code. The problem is that now you're guaranteed to have out-of-date tests, which no one will update to have complete coverage. A project without 100% complete test coverage is about as good as a project with no test coverage at all; so you're now left with a project with code written in half the time it should have been written in AND broken (or incomplete) tests.

### So what is important?
If you've got something you want done, get good developers and have someone to test the code. Most companies that have tests also have testers, which makes zero sense both logically and financially. A tester with a working brain is going to be a lot more valuable than a bunch of tests which you will not maintain or have complete coverage with. Scratch the tests and get good developers; a good developer will make sure that your code is clean and using best practices. The time not spent writing tests is time that can be spent improving your code base, your databases, and your overall project structure.
