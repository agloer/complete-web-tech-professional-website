---
title: "The Powers of the Wrapper App"
date: 2021-04-17T11:41:04-04:00
Description: ""
Tags: ["DIY", "App development", "Cordova", "Course"]
Categories: []
DisableComments: false
---

**We needed a game for iPhone and Android, and we needed it quickly.** We didn't have time for making a traditional app, and frankly, we didn't need to.

Many times, people believe that if you make a wrapper app, you're making big sacrifices. That's simply not the case. If your app is going to require internet access anyway, you are saving enormous amounts of time and money by using a wrapper app.

I made the Trivial AP US History App (with the help of an artist, of course!) in about a month. Click [here](/portfolio/trivialapush/) to see how it turned out!

If you're looking to do this yourself:
1. Determine what kind of app you will be making.
	- If it's a game, save yourself a week of development time, and use my [game wrapper](https://www.gitlab.com/agloer/trivial-apush-cordova).
	- If it's not, save yourself a week of development time, and use my [easy web app wrapper](https://www.gitlab.com/agloer/easy-web-app).
	- They're open source, licensed with the unlicense, so have fun, and use it however you like!
2. If you don't know what to do after you've cloned the git repository, take a look at my [course]({{< param "courseUrl" >}}).

If you're still struggling, or maybe you just don't want to tackle this project yourself, look me up on [upWork]({{< param "upWorkUrl" >}}) or send me an [email](mailto:{{< param "email" >}})!
