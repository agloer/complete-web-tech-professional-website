---
title: "Extending Functionality of WordPress Plugins"
date: 2021-03-27
tags: ["WordPress", "DIY", "Programming", "HTML", "Javascript", "CSS", "PHP"]
draft: false
---

Many times when working with WordPress, you will find that the plugin that you are using is missing a key piece of functionality that you require for your project. At that moment, you have the option of either hiring a professional, or adding the functionality yourself.

### I want to do it myself, what do I do?
The functionality you want will determine the next steps. Many times, it may be that you simply want to remove something that is visible from the page. For example, you may have a link that says "Show all categories" that you do not want. The first thing to check is always the settings for the plugin, if applicable. Before trying to reinvent the wheel, never forget to see if your plugin has settings, because they may have forseen your need!

#### Step 1: Check the settings of the plugin
Log in to your WordPress admin area. Locate the settings for your app, which should be on the left-hand side of the admin area. If it's not there, you may want to check Under Plugins->Installed Plugins.

#### Step 2: Modify CSS
If you can't find the modification in the settings for the app, we can move on to the easiest form of modification. In the case of our example of removing a small item from the page, we need to first open the page. Right-click on the undesired element and pull its class names or id for our next step.
![Open the Document Inspector](/img/inspect-an-element.png)
Now go into WP-Admin and click on "Appearance" on the left-hand side in the WP-Admin and customize your theme. In custom CSS, place a piece of code similar to the following:
{{< highlight css >}}
.some-element-class-name {
	display: none;
}
#or-maybe-some-element-id-name {
	display: none;
}
{{< /highlight >}}

#### Step 3: Find a plugin that does what you want
This sometimes may slip our mind, but perhaps there is a plugin that does accomplish what you want. Or perhaps you can get another plugin to work on top of your existing plugin. A common example is using GravityForms with the WooCommerce plugin. Before continuing down this path, make sure someone hasn't already done what you want!

#### Step 4: Add Javascript
The problem is now getting to be a bit more advanced, and unless you're comfortable using Javascript, you should consider hiring a professional at this point.

Identify the functionality you need, and write the script to extend the functionality you want. For example:
{{< highlight javascript >}}
function doSomething(event) {
	alert(`They clicked on the element!`);
}

const someElement = document.querySelector('#or-maybe-some-element-id-name');
someElement.addEventListener('click', 'doSomething');
{{< /highlight >}}

Now take your code, go into WP-Admin and click on "Pages" on the left-hand side of the admin tool. Locate your page, and in the code of the page, insert your new Javascript!
#### Step 5: Modify the plugin code
This is getting heavy. WordPress plugins are written in PHP. If you know how to program in PHP, read on!

The first step before writing any code is to see what the developers of the app themselves recommend. Visit the support page for the plugin that you will be extending. For example, [here is the support page for the color-filters plugin](https://wordpress.org/support/plugin/color-filters/). You will then want to SSH into your WordPress server and either modify the code directly, or copy it into your template folder and modify it there.

#### Step 6: Create your own plugin to extend the functionality (Very Advanced)
Sometimes you can't find the answer, and you'll need to start writing hooks attached to filters or actions taken by WordPress or the plugin itself. This requires making and adding your own plugin. This is beyond the scope of this article. If you find yourself on this step, or any other, in need of help, please [contact me](mailto:support@completewebtech.com).
