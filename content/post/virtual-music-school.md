---
title: "Virtual Music School"
date: 2021-02-26
tags: ["App development", "Cordova", "Ionic", "app", "success"]
draft: false
---

### Divine Inspiration
A few years ago, one of my friends, Tim, a youth pastor at my church in Brazil (where I spend a few months a year), was struggling a bit financially. I wanted to help him, but in a manner which would be sustainable. I wanted to give him an opportunity to make some more money on the side. As he was also an accomplished musician, who was the director of the church's school of music, I had the idea to make him a iPhone/Android app. There weren't any quality apps directed towards the Portuguese speaking world, and it seemed a space in which he would be able to make his mark.

He was enthusiastic with the idea, so I set to work. It took several months to design and develop the app in my spare time; the functionality the app needed constantly changed as we tested it out.

### Spreading the word
Tim didn't have money to spread his app, but he did have a number of friends. We immediately set out having all of his students and friends download the app. They would try it out and leave reviews. As the reviews and downloads grew, the app grew organically as well. It started to appear in searches in the app store, and through word of mouth, his success grew.

### Where it's at now
His app is among the top results when Portuguese speaking people search for music lessons for various instruments on the app stores. He has reached **well over 50,000 downloads** on the Google Play Store alone, with an **average 4.8 stars**. Though it is not an extremely lucrative venture for him at the moment, he is taking strides towards monetizing his efforts with merchandise and paid courses. Check out his app at the [link](https://play.google.com/store/apps/details?id=com.ionicframework.escolademusica), or look it up on your phone, "Escola de Música Virtual".
