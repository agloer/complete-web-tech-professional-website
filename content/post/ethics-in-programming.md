---
title: "Ethics in Programming"
date: 2021-07-04T18:34:41-04:00
Description: ""
Tags: ["Programming", "Ethics", "AI"]
DisableComments: false
---

So often the question in programming is not should we, but can we? Is it possible for us to accomplish the goal set before us? And with the advances in technology, the speed and cheapness of processing power, the answer has become more often than not, yes!

### Just because we can, doesn't mean that we should!
Many months ago, there was an opening for a developer for a COVID tracker app; one that would help do contact tracing for anyone who tested positive for COVID-19. Though the pay was good and well within my skill set, I did not apply and would never have accepted such a job. Regardless of whether the app is being made with good intentions, an app that can help track people for one reason can easily be repurposed for another.

Though not a zealot, I am a believer in free software (free as in freedom, and not as in free lunch), and the first core tenent is to preserve the freedom (and privacy) of the end user. Back in 2016, Richard Stallman (founder of GNU) had an excellent [talk](https://www.youtube.com/watch?v=n9YDz-Iwgyw) on the subject.

Every time there is a crisis, the state wastes no time to immediately push to take away the rights and privacy of the citizens. In 2001, with the terrorist attack in NY, we saw the overreach of the state in the form of the Patriot Act, and the ability to record and monitor all forms of communication of suspected terrorists. In 2020, with the worldwide pandemic, the state immediately worked to start tracking people through our personal trackers, cell phones. As programmers, we have a moral obligation to push back and show conscience over mercenarial greed.

### Morality is not limited to questions of privacy.
Another big issue I see today is the push to use AI for everything! I know it's exciting, and it's one of the fields that most excites me today. However, the careless pursuit of accomplishing all things has a moral element to it.

Consider a future where anything that a normal human being with normal intelligence could do, a robot could do better. What about a future where jobs in medicine, programming and investment are all done by machine? At what point do we stop and wonder if we've gone too far?

![GitHub Copilot screenshot](/img/GitHub-Copilot.png)
>Microsoft's GitHub Copilot is a wonderful convenience tool to improving programmer efficiency.

Thanks to programmers world-wide pushing their code to Microsoft's GitHub, we now have the wonderful convenience tool of GitHub Copilot. It provides us a glimpse into the future; a future where the computer can program additional functionality into itself. Just as self-driving cars started out as simply being able to automatically brake before a collision, we're on the same path towards the automation of the software engineering field. The same is being done in fields such as medicine and investment banking.

As a society, we'll all have to live with the consequences of what we as programmers have chosen to develop. When there are suddenly no jobs for 80% of the population to do, what then? When cars are self-driving, stock markets are nothing more than split-second purchases and sales made by AI, when visiting your doctor means standing in front of a screen, are we a human society any longer? When the morality of the machine depends on whether it was developed by Google or Facebook or Microsoft...what then?
