---
title: "About Complete Web Tech LLC"
date: 2021-02-23T11:49:29-05:00
draft: false
---

**Complete Web Tech LLC was founded in 2020. It consists of a single freelancer, Arthur Gloer.**

Hi, my name is Arthur Gloer. I have been a programmer for as long as I can remember. I have extensive experience in Python, PHP and MySQL as well as HTML5, CSS and Javascript.

If you have an existing WordPress website, I can help. I have experience working with WordPress websites, customizing them and extending plugin functionality.

Art is not my strong suit, but if you have a clear vision of exactly what you want and how you want it to look, I can make it happen. Through my family's business, InstantCert.com, I have had extensive experience with handling the business side of things; I can give you insight into how to hire the right freelancers for other aspects of your project. I can handle the aspects of managing a project, hiring web design freelancers, etc., if you need.

I'd like to talk to you before the project and maintain communication open throughout to ensure that I provide exactly what you are looking for. I am goal-oriented and meeting deadlines is very important to me. If you hire me, you can be sure that I will do everything possible to meet your expectations and deadlines. Receiving a 5-star feedback and your complete satisfaction is my primary goal.

{{< empty_space >}}

# Arthur Gloer
Freelance Full Stack Web Developer | [Upwork Profile](https://www.upwork.com/freelancers/~01648787fd43c845a8?s=1110580755107926016)

## Currently

* Senior Software Engineer, [InstantCert.com, LLC](https://www.instantcert.com/about) (2004 - )
* Chief Technical Officer, Value Labwork (2020 - )

### Technologies

* HTML5
* CSS
* Javascript (Angular, AngularJS, Node.JS)
* Python
* Linux systems administration
* Apache Web Server
* MySQL
* PHP
* WordPress

### Frameworks/Stacks

* Django (Preferred)
* CodeIgniter (LAMP stack)
* MEAN

### Passions

* Free and Open Source Software, especially GNU/Linux.

### Studying

* Artificial Intelligence
* AWS with Terraform

