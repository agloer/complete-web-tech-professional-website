---
title: "Spanishhour"
description: "PHP CodeIgniter project using Bootstrap for CSS."
thumbnail: "/img/spanish1.png"
tags: ["PHP", "Javascript", "MySQL", "LAMP", "CodeIgniter", "Bootstrap"]
weight: 0
draft: false
---

{{< empty_space >}}

![Main Page Screenshot](/img/spanish1.png)
>A decades old website needed an overhaul and migration to a whole new Google Cloud server. In order to make it responsive and usable on cellphones, I used Bootstrap for CSS. Migration was the perfect time to architect a whole new MySQL database before transfer of the old data.

{{< empty_space >}}

{{< empty_space >}}

![Members' Area Screenshot](/img/spanish2.png)
>The website needed new technology, as it was coded in PHP3. It was upgraded to the newest version of PHP and put on the CodeIgniter Framework.

{{< empty_space >}}

{{< empty_space >}}

![Speaking Practice Web App](/img/spanish3.png)
>Ancient Adobe Flash apps needed to be remade from scratch in responsive, modern JS.

{{< empty_space >}}
