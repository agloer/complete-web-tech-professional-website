---
title: "Trivial AP US History Android/iPhone Game"
description: "An educational Cordova Android/iPhone game that took a month to develop and launch."
thumbnail: "/img/trivial1.webp"
tags: ["App development", "Cordova", "Heroku", "MongoDB", "Express", "Node.JS", "Angular"]
weight: 0
draft: false
---

{{< empty_space >}}

![Main Menu Screenshot](/img/trivial1.webp)
>Trivial AP US History is an app to help high school students prepare for their AP US History exam.

{{< empty_space >}}

{{< empty_space >}}

![Leaderboard Screenshot](/img/trivial2.webp)
>Students can compete against each other globally, as well as see how they stack up against students from their own school!
>
>The app itself showcases the power of simple web wrapper app as taught in my [course]({{< param "courseUrl" >}}).

{{< empty_space >}}

{{< empty_space >}}

![Gameplay Screenshot](/img/trivial3.webp)
>The app wraps a MEAN (MongoDB, Express, Angular, Node.JS) stack application. The static Angular part of the website along with the Express/Node.JS backend sit on a Heroku instance, while the MongoDB sits on AWS managed by the MongoDB team.

[See the app on the Google Play Store!](https://play.google.com/store/apps/details?id=com.instantcert.trivialapush)

**The new version of the app is awaiting approval in the Apple App Store. The link will be added once it has been approved next week.**

{{< empty_space >}}
