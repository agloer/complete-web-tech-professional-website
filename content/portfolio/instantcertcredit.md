---
title: "InstantCertCredit Website"
description: "Python Django project using Bootstrap for CSS."
thumbnail: "/img/instantcertcredit.png"
tags: ["Python", "Javascript", "MySQL", "Django", "Bootstrap", "API"]
weight: 0
draft: false
---

{{< empty_space >}}

![Main Page Screenshot](/img/instantcertcredit.png)
>I was the architect of InstantCert Credit, a service which provides online college courses with proctored final exams.


{{< empty_space >}}

{{< empty_space >}}

![Members' Area Screenshot](/img/instantcertcredit2.png)
>A sampling of the ACE Credit reviewed courses.

{{< empty_space >}}

{{< empty_space >}}

![Members' Area Screenshot](/img/instantcertcredit3.png)
>Quizzes, timed and proctored final exams were all part of the solution.

{{< empty_space >}}

{{< empty_space >}}

![Speaking Practice Web App](/img/instantcertcredit4.png)
>School records programs were also required in the ACE Credit review process. I wrote the programs in Python, having them interact with the website via a custom API.

{{< empty_space >}}
