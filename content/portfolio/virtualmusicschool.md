---
title: "Virtual Music School Android/iPhone App"
description: "A mobile app made in Ionic Cordova with 50k+ users and 4.8 stars."
thumbnail: "/img/escola1.png"
tags: ["App development", "Python", "Ionic", "Cordova", "Apache", "Django", "API"]
weight: 0
draft: false
---

{{< empty_space >}}

![Main Page Screenshot](/img/escola1.png)
>Escola de Musica Virtual has had nearly **two thousand** reviews and 4.8 stars on the Google Play Store.

{{< empty_space >}}

{{< empty_space >}}

![Downloads Page](/img/escola3.webp)
>Users can download videos to watch offline at their leisure.

{{< empty_space >}}

{{< empty_space >}}

![Members' Area Screenshot](/img/escola2.png)
>As you can see, we have over 50 thousand users for the app!
>A Django backend supports the app, serving up videos to these users. I wrote the API and setup the Ubuntu Linux Apache server in the cloud.

[Read the story here!](/post/virtual-music-school/)

{{< empty_space >}}
